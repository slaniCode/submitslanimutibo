package tech.slani.org.asynctask;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ProgressBar;


import org.apache.http.client.HttpClient;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;

import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyStore;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import retrofit.RestAdapter;
import retrofit.client.ApacheClient;
import tech.slani.org.game.Game;
import tech.slani.org.httpclient.EasyHttpClient;
import tech.slani.org.repoobject.QuestionSet;
import tech.slani.org.restclient.RestClient;
import tech.slani.org.slanimutibo.R;
import tech.slani.org.slanimutibo.SlaniMutibo;

/**
 * Created by  on 16.11.2014.
 */
public class DownloadQuestionSet extends AsyncTask<String, Integer, List<QuestionSet>> {

    private final static String TAG = "DownloadQuestionSet.class";
    private WeakReference<Game> gameWeakReference;
    private int diffculty;


    public DownloadQuestionSet(Game activity, int difficulty) {

        gameWeakReference = new WeakReference<Game>(activity);
        this.diffculty = difficulty;
    }



    @Override
    protected List<QuestionSet> doInBackground(String... params) {

        RestClient restClientService = new RestAdapter.Builder()
                .setEndpoint(RestClient.URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build()
                .create(RestClient.class);

        SharedPreferences sharedPreferences = gameWeakReference.get()
                .getSharedPreferences(SlaniMutibo.PREFS_NAME, 0);


        List<QuestionSet> questionSets =
                restClientService.getQuestions(params[0]
                +"="+params[1], diffculty);

        return questionSets;
    }


    @Override
    protected void onPostExecute(List<QuestionSet> questionSets) {
        gameWeakReference.get().setQuestionSetList(questionSets);
    }
}
