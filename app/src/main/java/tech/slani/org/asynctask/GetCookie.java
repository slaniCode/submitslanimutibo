package tech.slani.org.asynctask;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.lang.ref.WeakReference;

import retrofit.RestAdapter;
import tech.slani.org.restclient.RestClient;
import tech.slani.org.slanimutibo.SlaniMutibo;

/**
 * Created by  on 24.11.2014.
 */
public class GetCookie extends AsyncTask<String, Void, Cookie> {

    private static final String COOKIE_URL =
            "https://slanimutibo.appspot.com/_ah/login?continue=http://localhost/&auth=";

    private WeakReference<SlaniMutibo> activityWeakReference;

    public GetCookie(SlaniMutibo activity) {

        activityWeakReference = new WeakReference<SlaniMutibo>(activity);
    }

    @Override
    protected Cookie doInBackground(String... params) {

        DefaultHttpClient httpClient = new DefaultHttpClient();
        try {
            httpClient.getParams().setBooleanParameter(ClientPNames.HANDLE_REDIRECTS, false);
            HttpGet httpGet = new HttpGet(COOKIE_URL + params[0]);
            HttpResponse httpResponse = httpClient.execute(httpGet);

            if (httpResponse.getStatusLine().getStatusCode() != 302) {
                return null;
            }
            for (Cookie cookie : httpClient.getCookieStore().getCookies()) {
                if (cookie.getName().equals("ACSID") || cookie.getName().equals("SACSID")) {
                    return cookie;
                }
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            httpClient.getParams().setBooleanParameter(ClientPNames.HANDLE_REDIRECTS, true);
        }

        return null;
    }

    @Override
    protected void onPostExecute(Cookie cookie) {

        activityWeakReference.get().setCookie(cookie);
    }
}
