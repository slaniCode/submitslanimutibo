package tech.slani.org.game;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.google.android.gms.plus.Plus;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import tech.slani.org.asynctask.DownloadQuestionSet;
import tech.slani.org.interfaces.SendQuestionToFragment;
import tech.slani.org.repoobject.QuestionResponse;
import tech.slani.org.repoobject.QuestionSet;
import tech.slani.org.service.ResponseService;
import tech.slani.org.slanimutibo.Play;
import tech.slani.org.slanimutibo.R;
import tech.slani.org.slanimutibo.SlaniMutibo;

public class Game extends ActionBarActivity implements
        QuestionFragment.OnFragmentInteractionListener,
        VoteFragment.VoteFragmentInteractionListener,
        PlusOneFragment.PlusFragmentInteractionListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final String VTAG = "votefragment";
    private static final String HTAG = "helpfragment";
    private static final String TAG = "Game.class";

    public SendQuestionToFragment sendQuestionToFragment;

    private List<QuestionSet> questionSetList;
    private ArrayList<QuestionResponse> questionResponses;
    private QuestionSet questionSet;
    private QuestionResponse questionResponse;

    private TextView points, wrong;
    private int nPoints, nWrong, help;

    private String cookie_name;
    private String cookie_value;

    private GoogleApiClient googleApiClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.relativeLayout, new QuestionFragment())
                    .commit();
        }

        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Plus.API)
                .addApi(Games.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .addScope(Plus.SCOPE_PLUS_PROFILE)
                .addScope(Games.SCOPE_GAMES)
                .addOnConnectionFailedListener(this)
                .addConnectionCallbacks(this)
                .build();

        SharedPreferences sharedPreferences =
                getSharedPreferences(SlaniMutibo.PREFS_NAME, 0);

        cookie_name = sharedPreferences.getString(SlaniMutibo.COOKIE_NAME, null);
        cookie_value = sharedPreferences.getString(SlaniMutibo.COOKIE_VALUE, null);
        Intent intent = getIntent();
        int difficulty = intent.getIntExtra(Play.DIFFICULTY_LEVEL, 100);
        DownloadQuestionSet downloadQuestionSet = new DownloadQuestionSet(this, difficulty);
        downloadQuestionSet.execute(cookie_name, cookie_value);
        questionResponses = new ArrayList<QuestionResponse>();

        nPoints = 0;
        nWrong = 0;
        help = 0;
        points = (TextView) findViewById(R.id.points);
        wrong = (TextView) findViewById(R.id.wrong);
        setPoints();
    }


    @Override
    protected void onStart() {
        super.onStart();
        googleApiClient.connect();
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_game, menu);
        menu.findItem(R.id.action_share).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            if (help < 3) {
                help++;
                help();
            }
        } else if (id == R.id.action_share) {
            shareAction();
        }

        return super.onOptionsItemSelected(item);
    }

    public void setQuestionSetList(List<QuestionSet> questionSetList) {
        this.questionSetList = questionSetList;
        sendQuestionSetToFragment();

    }

    private void help() {

        findViewById(R.id.relativeLayout2).setVisibility(View.VISIBLE);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.relativeLayout2, NeedHelpFragment.newInstance(questionSet.getFrequentAnswer(), questionSet.getQuestions()), HTAG).commit();

    }

    private void setPoints() {

        points.setText("" + nPoints);
        wrong.setText("" + nWrong);
    }

    public void sendQuestionSetToFragment() {
        if (!questionSetList.isEmpty() && nWrong > -3) {
            questionSet = questionSetList.remove(0);
            int[] i = questionSet.getAnswer();
            sendQuestionToFragment.sendQuestion(questionSet.getQuestions()
            );
        } else {
            ResponseService.startActionResponse(this, questionResponses,
                    cookie_name, cookie_value);
            Date date = Calendar.getInstance().getTime();

            ResponseService.startActionHistory(this, nPoints, nWrong, date.toString(),
                    cookie_name, cookie_value);

            sendDataToLeaderBoard();

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.relativeLayout, PlusOneFragment.newInstance(nPoints, nWrong))
                    .commit();
        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {

        int action = MotionEventCompat.getActionMasked(event);
        //Log.e(TAG, "onTouchEvent line" + 184 + " action" + action);
        return super.onTouchEvent(event);
    }

    @Override
    public void onFragmentInteraction(int i) {

        int[] answer = questionSet.getAnswer();

        nPoints += answer[i];
        nWrong = nWrong - (1 - answer[i]);
        questionResponse = new QuestionResponse();
        questionResponse.setKey(questionSet.getKey());
        if (answer[i] == 0) {
            questionResponse.setWrong(1);
        } else {
            questionResponse.setWrong(0);
        }
        questionResponse.setAnswer(i);
        setPoints();
        findViewById(R.id.relativeLayout2).setVisibility(View.VISIBLE);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.relativeLayout2,
                        VoteFragment.newInstance(questionSet.getExplanation(), answer[i]), VTAG)
                .commit();
    }


    @Override
    public void voteFragmentInteraction(int response) {


        questionResponse.setGrade(response);
        questionResponses.add(questionResponse);
        Fragment vote = getSupportFragmentManager().findFragmentByTag(VTAG);
        if (vote != null) {
            getSupportFragmentManager().beginTransaction().remove(vote).commit();
        }
        findViewById(R.id.relativeLayout2).setVisibility(View.GONE);
        sendQuestionSetToFragment();
    }


    @Override
    public void plusFragmentInteraction(Uri uri) {

    }

    private void shareAction() {

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, nPoints + " " + getResources().getString(R.string.share_text));
        intent.setType("text/plain");
        startActivity(Intent.createChooser(intent, getResources().getText(R.string.send_to)));
    }

    private void sendDataToLeaderBoard() {

        if (googleApiClient.isConnected()) {
            Games.Leaderboards.submitScore(googleApiClient, SlaniMutibo.LEADERBOARD, nPoints);
        }
    }

    @Override
    public void onConnected(Bundle bundle) {

    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}
