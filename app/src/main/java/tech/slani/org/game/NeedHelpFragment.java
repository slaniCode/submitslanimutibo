package tech.slani.org.game;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import tech.slani.org.slanimutibo.R;


public class NeedHelpFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private int[] mParam1;
    private String[] mParam2;

    private TextView title1, n1;
    private TextView title2, n2;
    private TextView title3, n3;
    private TextView title4, n4;



    public static NeedHelpFragment newInstance(int[] param1, String[] param2) {
        NeedHelpFragment fragment = new NeedHelpFragment();
        Bundle args = new Bundle();
        args.putIntArray(ARG_PARAM1, param1);
        args.putStringArray(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public NeedHelpFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getIntArray(ARG_PARAM1);
            mParam2 = getArguments().getStringArray(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_need_help, container, false);

        title1 = (TextView) view.findViewById(R.id.textView3);
        n1 = (TextView) view.findViewById(R.id.textView4);
        title2 = (TextView) view.findViewById(R.id.textView5);
        n2 = (TextView) view.findViewById(R.id.textView6);
        title3 = (TextView) view.findViewById(R.id.textView7);
        n3 = (TextView) view.findViewById(R.id.textView8);
        title4 = (TextView) view.findViewById(R.id.textView9);
        n4 = (TextView) view.findViewById(R.id.textView10);

        title1.setText(mParam2[0]);
        title2.setText(mParam2[1]);
        title3.setText(mParam2[2]);
        title4.setText(mParam2[3]);

        n1.setText(""+mParam1[0]);
        n2.setText(""+mParam1[1]);
        n3.setText(""+mParam1[2]);
        n4.setText(""+mParam1[3]);

        return view;
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }




}
