package tech.slani.org.game;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import tech.slani.org.slanimutibo.R;

import com.google.android.gms.plus.PlusOneButton;
import com.google.android.gms.plus.PlusShare;


public class PlusOneFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private int nPoints;
    private int nWrong;

    private TextView points;
    private TextView wrong;

    private PlusFragmentInteractionListener mListener;


    public static PlusOneFragment newInstance(int points, int wrong) {
        PlusOneFragment fragment = new PlusOneFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, points);
        args.putInt(ARG_PARAM2, wrong);
        fragment.setArguments(args);
        return fragment;
    }

    public PlusOneFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            nPoints = getArguments().getInt(ARG_PARAM1);
            nWrong = getArguments().getInt(ARG_PARAM2);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_plus_one, container, false);



        points = (TextView) view.findViewById(R.id.text_points);
        wrong = (TextView) view.findViewById(R.id.text_wrong);
        points.setText(""+nPoints);
        wrong.setText(""+nWrong);


        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.plusFragmentInteraction(uri);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        menu.findItem(R.id.action_share).setVisible(true);
        menu.findItem(R.id.action_settings).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (PlusFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface PlusFragmentInteractionListener {
        public void plusFragmentInteraction(Uri uri);
    }

}
