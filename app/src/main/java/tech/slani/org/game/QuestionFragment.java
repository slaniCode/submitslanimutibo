package tech.slani.org.game;

import android.app.Activity;
import android.content.SharedPreferences;
import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.gesture.Prediction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.GestureDetectorCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

import tech.slani.org.interfaces.SendQuestionToFragment;
import tech.slani.org.service.ResponseService;
import tech.slani.org.slanimutibo.R;
import tech.slani.org.slanimutibo.SlaniMutibo;

public class QuestionFragment extends Fragment implements
        SendQuestionToFragment, GestureOverlayView.OnGesturePerformedListener {

    private static final String TAG = "QuestionFragment.class";

    private OnFragmentInteractionListener mListener;

    private Button button1, button2, button3, button4;

    private GestureDetectorCompat gestureDetectorCompat;
    private GestureLibrary gLibrary;
    private int gestureViewID1, gestureViewID2, gestureViewID3, gestureViewID4;

    private String cookie_name;
    private String cookie_value;

    private String[] questions;

    public static QuestionFragment newInstance() {
        QuestionFragment fragment = new QuestionFragment();
        return fragment;
    }

    public QuestionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        gLibrary = GestureLibraries.fromRawResource(getActivity(), R.raw.gestures);
        if (!gLibrary.load()) {
            Log.e(TAG, "glibary not loaded");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_question, container, false);

        button1 = (Button) view.findViewById(R.id.button1);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setButtonClilck(false);
                sendToActivity(0);

            }
        });
        button2 = (Button) view.findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setButtonClilck(false);
                sendToActivity(1);
            }
        });
        button3 = (Button) view.findViewById(R.id.button3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setButtonClilck(false);
                sendToActivity(2);

            }
        });
        button4 = (Button) view.findViewById(R.id.button4);
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setButtonClilck(false);
                sendToActivity(3);
            }
        });

        setButtonClilck(false);

        // Make this the target of gesture detection callbacks
        GestureOverlayView gestureView1 = (GestureOverlayView) view.findViewById(R.id.gestures_overlay1);
        GestureOverlayView gestureView2 = (GestureOverlayView) view.findViewById(R.id.gestures_overlay2);
        GestureOverlayView gestureView3 = (GestureOverlayView) view.findViewById(R.id.gestures_overlay3);
        GestureOverlayView gestureView4 = (GestureOverlayView) view.findViewById(R.id.gestures_overlay4);

        gestureViewID1 = gestureView1.getId();
        gestureViewID2 = gestureView2.getId();
        gestureViewID3 = gestureView3.getId();
        gestureViewID4 = gestureView4.getId();
        gestureView1.addOnGesturePerformedListener(this);
        gestureView2.addOnGesturePerformedListener(this);
        gestureView3.addOnGesturePerformedListener(this);
        gestureView4.addOnGesturePerformedListener(this);

        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    public void sendToActivity(int i) {
        if (mListener != null) {
            mListener.onFragmentInteraction(i);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }

        ((Game) getActivity()).sendQuestionToFragment = this;

        SharedPreferences sharedPreferences =
                getActivity().getSharedPreferences(SlaniMutibo.PREFS_NAME, 0);
        cookie_name = sharedPreferences.getString(SlaniMutibo.COOKIE_NAME, null);
        cookie_value = sharedPreferences.getString(SlaniMutibo.COOKIE_VALUE, null);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public void sendQuestion(String[] questions) {
        this.questions = questions;
        setButtonText();
        setButtonClilck(true);

    }

    private void setButtonText() {

        button1.setText(questions[0]);
        button2.setText(questions[1]);
        button3.setText(questions[2]);
        button4.setText(questions[3]);
    }

    private void setButtonClilck(boolean bool) {

        button1.setClickable(bool);
        button2.setClickable(bool);
        button3.setClickable(bool);
        button4.setClickable(bool);
    }

    @Override
    public void onGesturePerformed(GestureOverlayView overlay, Gesture gesture) {
        // Get gesture predictions
        ArrayList<Prediction> predictions = gLibrary.recognize(gesture);

        // Get highest-ranked prediction
        if (predictions.size() > 0) {
            Prediction prediction = predictions.get(0);

            if (prediction.score > 3.0) {

                addWishlist(overlay.getId());
            }

        }
    }

    private void addWishlist(int id) {

        if (id == gestureViewID1) {
            ResponseService.startActionWishlist(getActivity(), ResponseService.ADD_TITLE,
                    questions[0], cookie_name, cookie_value);
            Toast.makeText(getActivity(), questions[0] + "was added to wish list",
                    Toast.LENGTH_SHORT).show();
        } else if (id == gestureViewID2) {
            ResponseService.startActionWishlist(getActivity(), ResponseService.ADD_TITLE,
                    questions[1], cookie_name, cookie_value);
            Toast.makeText(getActivity(), questions[1] + "was added to wish list",
                    Toast.LENGTH_SHORT).show();

        } else if (id == gestureViewID3) {
            ResponseService.startActionWishlist(getActivity(), ResponseService.ADD_TITLE,
                    questions[2], cookie_name, cookie_value);
            Toast.makeText(getActivity(), questions[2] + "was added to wish list",
                    Toast.LENGTH_SHORT).show();

        } else if (id == gestureViewID4) {
            ResponseService.startActionWishlist(getActivity(), ResponseService.ADD_TITLE,
                    questions[4], cookie_name, cookie_value);
            Toast.makeText(getActivity(), questions[4] + "was added to wish list",
                    Toast.LENGTH_SHORT).show();

        }
    }


    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(int i);
    }

}
