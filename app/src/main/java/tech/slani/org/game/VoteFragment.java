package tech.slani.org.game;

import android.app.Activity;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import tech.slani.org.slanimutibo.R;

public class VoteFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private static final int GOOD = 1;
    private static final int BAD = -1;

    private String answer;
    private int i;

    private VoteFragmentInteractionListener mListener;

    public static VoteFragment newInstance(String param1, int i) {
        VoteFragment fragment = new VoteFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putInt(ARG_PARAM2, i);
        fragment.setArguments(args);
        return fragment;
    }

    public VoteFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            answer = getArguments().getString(ARG_PARAM1);
            i = getArguments().getInt(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_vote, container, false);

        TextView textView = (TextView) view.findViewById(R.id.explanation);
        if (i == 0) {

            textView.setTextColor(Color.parseColor("#CC0000"));
        }
        textView.setText(answer);

        ImageButton good = (ImageButton) view.findViewById(R.id.imageGood);
        ImageButton bad = (ImageButton) view.findViewById(R.id.imageBad);

        bad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResponse(BAD);
            }
        });

        good.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResponse(GOOD);
            }
        });

        return view;
    }

    public void setResponse(int response) {
        if (mListener != null) {
            mListener.voteFragmentInteraction(response);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (VoteFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface VoteFragmentInteractionListener {
        public void voteFragmentInteraction(int response);
    }

}
