package tech.slani.org.interfaces;


/**
 * Created by slani on 16.11.2014.
 */
public interface SendQuestionToFragment {
    public void sendQuestion(String[] questions);
}
