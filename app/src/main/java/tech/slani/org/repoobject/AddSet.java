package tech.slani.org.repoobject;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by slani on 1.12.2014.
 */
public class AddSet implements Parcelable {


    String title1, title2, title3, title4;
    String explanation;
    int answer;

    public String getTitle1() {
        return title1;
    }

    public void setTitle1(String title1) {
        this.title1 = title1;
    }

    public String getTitle2() {
        return title2;
    }

    public void setTitle2(String title2) {
        this.title2 = title2;
    }

    public String getTitle3() {
        return title3;
    }

    public void setTitle3(String title3) {
        this.title3 = title3;
    }

    public String getTitle4() {
        return title4;
    }

    public void setTitle4(String title4) {
        this.title4 = title4;
    }

    public String getExplanation() {
        return explanation;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public int getAnswer() {
        return answer;
    }

    public void setAnswer(int answer) {
        this.answer = answer;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(title1);
        dest.writeString(title2);
        dest.writeString(title3);
        dest.writeString(title4);
        dest.writeString(explanation);
        dest.writeInt(answer);

    }

    public static final Parcelable.Creator<AddSet> CREATOR
            = new Parcelable.Creator<AddSet>() {
        public AddSet createFromParcel(Parcel in) {
            return new AddSet(in);
        }

        public AddSet[] newArray(int size) {
            return new AddSet[size];
        }
    };

    private AddSet(Parcel in) {
        title1 = in.readString();
        title2 = in.readString();
        title3 = in.readString();
        title4 = in.readString();
        explanation = in.readString();
        answer = in.readInt();
    }

    public AddSet () {

    }
}
