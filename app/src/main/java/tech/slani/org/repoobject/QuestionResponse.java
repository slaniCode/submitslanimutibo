package tech.slani.org.repoobject;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by slani on 30.10.2014.
 */
public class QuestionResponse implements Parcelable {

    private String key;
    private int wrong, grade, answer;

    public void setKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public int getWrong() {
        return wrong;
    }

    public void setWrong(int wrong) {
        this.wrong = wrong;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public int getAnswer() {
        return answer;
    }

    public void setAnswer(int answer) {
        this.answer = answer;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(key);
        dest.writeInt(wrong);
        dest.writeInt(grade);
        dest.writeInt(answer);

    }

    public static final Parcelable.Creator<QuestionResponse> CREATOR
            = new Parcelable.Creator<QuestionResponse>() {
        public QuestionResponse createFromParcel(Parcel in) {
            return new QuestionResponse(in);
        }

        public QuestionResponse[] newArray(int size) {
            return new QuestionResponse[size];
        }
    };

    private QuestionResponse(Parcel in) {
        key = in.readString();
        wrong = in.readInt();
        grade = in.readInt();
        answer = in.readInt();
    }

    public QuestionResponse () {

    }


}
