package tech.slani.org.repoobject;


/**
 * Created by slani on 19.10.2014.
 */
public class QuestionSet {


    private String key;
    private int all, wrong, diffculty;
    private int grade;          // if grade == - 10 {delete};
    private int[] answer;
    private int[] frequentAnswer;
    private String explanation;
    private String[] questions;


    public String getKey() {
        return key;
    }

    public void String (String key) {
        this.key = key;
    }

    public int getAll() {
        return all;
    }

    public void setAll(int all) {
        this.all = all;
    }

    public int getWrong() {
        return wrong;
    }

    public void setWrong(int wrong) {
        this.wrong = wrong;
    }

    public int getDiffculty() {
        return diffculty;
    }

    public void setDiffculty(int diffculty) {
        this.diffculty = diffculty;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public int[] getAnswer() {
        return answer;
    }

    public void setAnswer(int[] answer) {
        this.answer = answer;
    }

    public int[] getFrequentAnswer() {
        return frequentAnswer;
    }

    public void setFrequentAnswer(int[] frequentAnswer) {
        this.frequentAnswer = frequentAnswer;
    }

    public String[] getQuestions() {
        return questions;
    }

    public void setQuestions(String[] questions) {
        this.questions = questions;
    }

    public String getExplanation() {
        return explanation;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public void incrmentFrequentAnswer(int i) {
        frequentAnswer[i]++;
    }
}
