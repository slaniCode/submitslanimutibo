package tech.slani.org.restclient;


import org.apache.http.cookie.Cookie;

import java.util.ArrayList;
import java.util.List;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import tech.slani.org.repoobject.AddSet;
import tech.slani.org.repoobject.QuestionResponse;
import tech.slani.org.repoobject.QuestionSet;
import tech.slani.org.repoobject.UserHistory;

/**
 * Created by slani on 16.11.2014.
 */
public interface RestClient {

    public static final String URL = "https://slanimutibo.appspot.com/";
    public static final String PATH = "/";
    public static final String QUESTION_PATH = "/question/";
    public static final String  WISHLIST_PATH = "/wishlist/";

    @GET(PATH)
    public String addVideo(@Header("Cookie") String cookie);
    //public String addVideo();

    @POST(QUESTION_PATH + "get")
    public List<QuestionSet> getQuestions(@Header("Cookie") String cookie, @Body int difficulty);

    @POST(QUESTION_PATH + "addset")
    public String addSet(@Header("Cookie") String cookie, @Body AddSet addSet);

    @POST(QUESTION_PATH + "response")
    public String response(@Header("Cookie") String cookie, @Body List<QuestionResponse> questionResponses);

    @POST(QUESTION_PATH + "addhistory")
    public String addHistory(@Header("Cookie") String cookie, @Body UserHistory userHistory);

    @GET(QUESTION_PATH +  "gethistory")
    public List<UserHistory> getHistory(@Header("Cookie") String cookie);

    // WishList stuff
    @POST(WISHLIST_PATH + "addtitle")
    public String addTitle(@Header("Cookie") String cookie, @Body String title);

    @POST(WISHLIST_PATH + "removetitle")
    public String removeTitle(@Header("Cookie") String cookie, @Body String title);

    @GET(WISHLIST_PATH + "removewishlist")
    String removeWishList(@Header("Cookie") String cookie);

    @GET(WISHLIST_PATH + "getwishlist")
    public ArrayList<String> getWishList(@Header("Cookie") String cookie);
}

