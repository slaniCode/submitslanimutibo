package tech.slani.org.service;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.LinkedList;

import retrofit.RestAdapter;
import tech.slani.org.repoobject.AddSet;
import tech.slani.org.repoobject.QuestionResponse;
import tech.slani.org.repoobject.UserHistory;
import tech.slani.org.restclient.RestClient;

public class ResponseService extends IntentService {

    public static final int ADD_TITLE = 0;
    public static final int REMOVE_TITLE = 1;
    public static final int REMOVE_WISHLIST = 2;



    private static final String TAG = "tech.slani.org.service.action.";
    private static final String ACTION_RESPONSE = "tech.slani.org.service.action.RESPONSE";
    private static final String ACTION_HISTORY = "tech.slani.org.service.action.HISTORY";
    private static final String ACTION_WISHLIST = "tech.slani.org.service.action.WISHLIST";
    private static final String ACTION_ADDSET = "tech.slani.org.service.action.ADDSET";



    private static final String EXTRA_RESPONSE = "tech.slani.org.service.extra.RESPONSE";
    private static final String EXTRA_COOKIE_NAME = "tech.slani.org.service.extra.COOKIE_NAME";
    private static final String EXTRA_COOKIE_VALUE = "tech.slani.org.service.extra.COOKIE_VALUE";
    private static final String EXTRA_POINTS = "tech.slani.org.service.extra.POINTS";
    private static final String EXTRA_WRONG = "tech.slani.org.service.extra.WRONG";
    private static final String EXTRA_DATE = "tech.slani.org.service.extra.DATE";
    private static final String EXTRA_TITLE = "tech.slani.org.service.extra.TITLE";
    private static final String EXTRA_ACTION = "tech.slani.org.service.extra.ACTION";
    private static final String EXTRA_SET = "tech.slani.org.service.extra.SET";

    public static void startActionAddSet(Context context, ArrayList<AddSet> addSet,
                                           String cookie_name, String cookie_value) {

        Intent intent = new Intent(context, ResponseService.class);
        intent.setAction(ACTION_ADDSET);
        intent.putParcelableArrayListExtra(EXTRA_SET, addSet);
        intent.putExtra(EXTRA_COOKIE_NAME, cookie_name);
        intent.putExtra(EXTRA_COOKIE_VALUE, cookie_value);
        context.startService(intent);
    }

    public static void startActionWishlist(Context context, int action, String title,
                                           String cookie_name, String cookie_value) {

        Intent intent = new Intent(context, ResponseService.class);
        intent.setAction(ACTION_WISHLIST);
        intent.putExtra(EXTRA_ACTION, action);
        intent.putExtra(EXTRA_TITLE, title);
        intent.putExtra(EXTRA_COOKIE_NAME, cookie_name);
        intent.putExtra(EXTRA_COOKIE_VALUE, cookie_value);
        context.startService(intent);
    }
    public static void startActionResponse(Context context, ArrayList<QuestionResponse> questionResponses,
                                           String cookie_name, String cookie_value) {
        Intent intent = new Intent(context, ResponseService.class);
        intent.setAction(ACTION_RESPONSE);
        intent.putParcelableArrayListExtra(EXTRA_RESPONSE, questionResponses);
        intent.putExtra(EXTRA_COOKIE_NAME, cookie_name);
        intent.putExtra(EXTRA_COOKIE_VALUE, cookie_value);
        context.startService(intent);
    }

    public static void startActionHistory(Context context, int points, int wrong, String date,
                                          String cookie_name, String cookie_value) {

        Intent intent = new Intent(context, ResponseService.class);
        intent.setAction(ACTION_HISTORY);
        intent.putExtra(EXTRA_POINTS, points);
        intent.putExtra(EXTRA_WRONG, wrong);
        intent.putExtra(EXTRA_DATE, date);
        intent.putExtra(EXTRA_COOKIE_NAME, cookie_name);
        intent.putExtra(EXTRA_COOKIE_VALUE, cookie_value);
        context.startService(intent);
    }

    public ResponseService() {
        super("ResponseService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_RESPONSE.equals(action)) {
                final ArrayList<QuestionResponse> questionResponses
                        = intent.getParcelableArrayListExtra(EXTRA_RESPONSE);
                final String cooke_name = intent.getStringExtra(EXTRA_COOKIE_NAME);
                final String cooke_value = intent.getStringExtra(EXTRA_COOKIE_VALUE);
                handleActionResponse(questionResponses, cooke_name, cooke_value);
            } else if(ACTION_HISTORY.equals(action)) {
                final int points = intent.getIntExtra(EXTRA_POINTS, 0);
                final int wrong = intent.getIntExtra(EXTRA_WRONG, 0);
                final String date = intent.getStringExtra(EXTRA_DATE);
                final String cooke_name = intent.getStringExtra(EXTRA_COOKIE_NAME);
                final String cooke_value = intent.getStringExtra(EXTRA_COOKIE_VALUE);
                handleActionHistory(points, wrong, date, cooke_name, cooke_value);
            } else if(ACTION_WISHLIST.equals(action)) {
                final int actionint = intent.getIntExtra(EXTRA_ACTION, 3);
                final String title = intent.getStringExtra(EXTRA_TITLE);
                final String cooke_name = intent.getStringExtra(EXTRA_COOKIE_NAME);
                final String cooke_value = intent.getStringExtra(EXTRA_COOKIE_VALUE);
                handleActionWishList(actionint, title, cooke_name,cooke_value);
            } else if(ACTION_ADDSET.equals(action)) {
                final ArrayList<AddSet> addSet = intent.getParcelableArrayListExtra(EXTRA_SET);
                final String cooke_name = intent.getStringExtra(EXTRA_COOKIE_NAME);
                final String cooke_value = intent.getStringExtra(EXTRA_COOKIE_VALUE);
                handleActionAddSet(addSet.get(0), cooke_name, cooke_value);
            }

        }
    }

    private void handleActionAddSet(AddSet addSet, String cookie_name, String cookie_value) {
        RestClient restClientService = new RestAdapter.Builder()
                .setEndpoint(RestClient.URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build()
                .create(RestClient.class);

        String ok = restClientService.addSet(cookie_name + "=" + cookie_value, addSet);

    }

    private void handleActionWishList(int action, String title, String cookie_name, String cookie_value) {

        RestClient restClientService = new RestAdapter.Builder()
                .setEndpoint(RestClient.URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build()
                .create(RestClient.class);

        switch (action) {
            case ADD_TITLE:
                String ok = restClientService.addTitle(cookie_name + "=" + cookie_value, title);
                return;
            case REMOVE_TITLE:
                String ok1 = restClientService.removeTitle(cookie_name + "=" + cookie_value, title);
                return;
            case REMOVE_WISHLIST:
                String ok2 = restClientService.removeWishList(cookie_name + "=" + cookie_value);
                return;
        }

    }

    private void handleActionHistory(int points, int wrong, String date, String cookie_name, String cookie_value) {

        RestClient restClientService = new RestAdapter.Builder()
                .setEndpoint(RestClient.URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build()
                .create(RestClient.class);

        UserHistory userHistory = new UserHistory();
        userHistory.setPoints(points);
        userHistory.setWrong(wrong);
        userHistory.setDate(date);
        userHistory.setQuestionNumber(points - (wrong));
        String ok = restClientService.addHistory(cookie_name+"="+cookie_value, userHistory);
    }

    private void handleActionResponse(ArrayList<QuestionResponse> questionResponses,
                                      String cookie_name, String cookie_value) {
        RestClient restClientService = new RestAdapter.Builder()
                .setEndpoint(RestClient.URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build()
                .create(RestClient.class);


        String ok = restClientService.response(cookie_name+"="+cookie_value, questionResponses);
    }
}
