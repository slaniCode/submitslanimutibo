package tech.slani.org.slanimutibo;

import android.app.Activity;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;

import java.util.ArrayList;

import tech.slani.org.repoobject.AddSet;
import tech.slani.org.service.ResponseService;

public class AddQuestionFragment extends Fragment {

    private EditText text1, text2, text3, text4, text5;
    private NumberPicker numberPicker;
    private Button button;

    private static final int TOAST_ADD = 1;
    private static final int TOAST_NUMBER = 2;
    private static final int TOAST_FIELDS = 3;

    public static AddQuestionFragment newInstance() {
        AddQuestionFragment fragment = new AddQuestionFragment();

        return fragment;
    }

    public AddQuestionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_add_question, container, false);

        text1 = (EditText) view.findViewById(R.id.editText);
        text2 = (EditText) view.findViewById(R.id.editText2);
        text3 = (EditText) view.findViewById(R.id.editText3);
        text4 = (EditText) view.findViewById(R.id.editText4);
        text5 = (EditText) view.findViewById(R.id.editText5);

        button = (Button) view.findViewById(R.id.button5);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNew();

            }
        });
        numberPicker = (NumberPicker) view.findViewById(R.id.numberPicker);
        numberPicker.setMaxValue(4);
        numberPicker.setMinValue(0);
        numberPicker.setValue(0);



        return view;
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((SlaniMutibo) activity).onSectionAttached(3);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void addNew() {

        String title1 = text1.getText().toString();
        if (checkString(title1)) {
            showToast(TOAST_FIELDS);
            return;
        }
        String title2= text2.getText().toString();
        if (checkString(title2)) {
            showToast(TOAST_FIELDS);
            return;
        }
        String title3 = text3.getText().toString();
        if (checkString(title3)) {
            showToast(TOAST_FIELDS);
            return;
        }
        String title4 = text4.getText().toString();
        if (checkString(title4)) {
            showToast(TOAST_FIELDS);
            return;
        }

        String title5 = text5.getText().toString();
        if (checkString(title5)) {
            showToast(TOAST_FIELDS);
            return;
        }

        int i = numberPicker.getValue();
        if (i == 0) {
            showToast(TOAST_NUMBER);
            return;
        }

        AddSet addSet = new AddSet();

        addSet.setTitle1(title1);
        addSet.setTitle2(title2);
        addSet.setTitle3(title3);
        addSet.setTitle4(title4);
        addSet.setExplanation(title5);
        addSet.setAnswer(i);

        ArrayList<AddSet> addSets = new ArrayList<AddSet>();
        addSets.add(addSet);

        Activity activity = getActivity();
        SharedPreferences sharedPreferences =
                activity.getSharedPreferences(SlaniMutibo.PREFS_NAME, 0);
        ResponseService.startActionAddSet(activity, addSets,
                sharedPreferences.getString(SlaniMutibo.COOKIE_NAME, null),
                sharedPreferences.getString(SlaniMutibo.COOKIE_VALUE, null));


        erase();
        showToast(TOAST_ADD);
    }

    private boolean checkString(String string) {

        if (string.equals("")) {
            return true;
        }

        return false;
    }


    private void erase() {

        text1.setText("");
        text2.setText("");
        text3.setText("");
        text4.setText("");
        text5.setText("");
        numberPicker.setValue(0);
    }

    private void showToast(int i) {

        Toast toast;

        switch (i) {
            case 1:
                toast = Toast.makeText(getActivity(), "Question set has been added",
                        Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                break;
            case 2:
                toast = Toast.makeText(getActivity(), "Choose different movie!",
                        Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            default:
                toast = Toast.makeText(getActivity(), "Fill in all the fields!",
                        Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
        }
    }
}
