package tech.slani.org.slanimutibo;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import retrofit.RestAdapter;
import tech.slani.org.repoobject.UserHistory;
import tech.slani.org.restclient.RestClient;


public class HistoryFragment extends Fragment implements AbsListView.OnItemClickListener,
        LoaderManager.LoaderCallbacks<List<UserHistory>> {

    private static final String TAG = "HistoryFragment.class";
    /**
     * The fragment's ListView/GridView.
     */
    private ListView mListView;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private HistoryAdapter mAdapter;


    static class ViewHolder {
        public TextView points, date, wrong;
    }

    public static HistoryFragment newInstance() {

        HistoryFragment fragment = new HistoryFragment();
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public HistoryFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAdapter = new HistoryAdapter(getActivity(), R.layout.custom_history_row);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item, container, false);

        // Set the adapter
        mListView = (ListView) view.findViewById(android.R.id.list);
        mListView.setAdapter(mAdapter);

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        ((SlaniMutibo) activity).onSectionAttached(2);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    /**
     * The default content for this Fragment has a TextView that is shown when
     * the list is empty. If you would like to change the text, call this method
     * to supply the text it should use.
     */
    public void setEmptyText(CharSequence emptyText) {
        View emptyView = mListView.getEmptyView();

        if (emptyView instanceof TextView) {
            ((TextView) emptyView).setText(emptyText);
        }
    }


    private class HistoryAdapter extends ArrayAdapter<UserHistory> {

        private final Activity activity;


        public HistoryAdapter(Activity activity, int resource) {
            super(activity, resource);
            this.activity = activity;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View view = convertView;

            if (view == null) {
                LayoutInflater inflater = activity.getLayoutInflater();
                view = inflater.inflate(R.layout.custom_history_row, null);
                ViewHolder viewHolder = new ViewHolder();
                viewHolder.points = (TextView) view.findViewById(R.id.text_points);
                viewHolder.date = (TextView) view.findViewById(R.id.text_date);
                viewHolder.wrong = (TextView) view.findViewById(R.id.text_wrong);
                view.setTag(viewHolder);
            }

            ViewHolder viewHolder = (ViewHolder) view.getTag();
            UserHistory userHistory = getItem(position);
            viewHolder.points.setText("" + userHistory.getPoints());
            viewHolder.date.setText(userHistory.getDate());
            viewHolder.wrong.setText("" + userHistory.getWrong());

            return view;
        }
    }

    @Override
    public Loader<List<UserHistory>> onCreateLoader(int i, Bundle bundle) {
        return new HistoryLoader(getActivity().getApplicationContext());
    }

    @Override
    public void onLoadFinished(Loader<List<UserHistory>> listLoader, List<UserHistory> userHistories) {
        addAll(userHistories);
    }

    @Override
    public void onLoaderReset(Loader<List<UserHistory>> listLoader) {
        addAll(null);
    }

    private void addAll(List<UserHistory> userHistories) {

        mAdapter.clear();
        if (userHistories != null) {
            for (UserHistory h : userHistories) {
                mAdapter.add(h);
            }
        }
    }

    private static class HistoryLoader extends AsyncTaskLoader<List<UserHistory>> {

        final InterestingConfigChanges mLastConfig = new InterestingConfigChanges();

        List<UserHistory> userHistories;
        private Context context;

        public HistoryLoader(Context context) {
            super(context);
            this.context = context;
        }

        @Override
        public List<UserHistory> loadInBackground() {

            RestClient restClientService = new RestAdapter.Builder()
                    .setEndpoint(RestClient.URL)
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .build()
                    .create(RestClient.class);

            SharedPreferences sharedPreferences =
                    context.getSharedPreferences(SlaniMutibo.PREFS_NAME, 0);

            userHistories = restClientService.getHistory(
                    sharedPreferences.getString(SlaniMutibo.COOKIE_NAME, null) + "=" +
                            sharedPreferences.getString(SlaniMutibo.COOKIE_VALUE, null));


            return userHistories;
        }

        @Override
        public void deliverResult(List<UserHistory> data) {

            if (isReset()) {
                if (data != null) {
                    onReleaseResources(data);
                }
            }

            List<UserHistory> oldUserHistories = userHistories;
            userHistories = data;

            if (isStarted()) {
                super.deliverResult(data);
            }

            if (oldUserHistories != null) {
                onReleaseResources(oldUserHistories);
            }
        }

        @Override
        protected void onStartLoading() {

            if (userHistories != null) {
                deliverResult(userHistories);
            }

            boolean configChange = mLastConfig.applyNewConfig(getContext().getResources());

            if (takeContentChanged() || userHistories == null || configChange) {
                forceLoad();
            }


        }

        @Override
        protected void onStopLoading() {
            cancelLoad();
        }

        @Override
        public void onCanceled(List<UserHistory> data) {
            super.onCanceled(data);

            onReleaseResources(data);
        }

        @Override
        protected void onReset() {
            super.onReset();

            onStartLoading();

            if (userHistories != null) {
                onReleaseResources(userHistories);
                userHistories = null;
            }
        }

        protected void onReleaseResources(List<UserHistory> data) {
        }


    }

}
