package tech.slani.org.slanimutibo;

import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.google.android.gms.plus.Plus;

import tech.slani.org.game.Game;


public class PlusSignInActivity extends ActionBarActivity implements GoogleApiClient.ConnectionCallbacks,
GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {


    private static final String TAG = "PlusSignInActivity.class";
    private static final int RC_SIGN_IN = 0;

    private GoogleApiClient googleApiClient;

    private boolean signInClicked;
    private boolean mIntentInProgress;

    private SignInButton signInButton;
    private ConnectionResult mConnectionResult;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plus_sign_in);

        googleApiClient = new GoogleApiClient.Builder(this)
                .addOnConnectionFailedListener(this)
                .addConnectionCallbacks(this)
                .addApi(Plus.API)
                .addApi(Games.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .addScope(Plus.SCOPE_PLUS_PROFILE)
                .addScope(Games.SCOPE_GAMES)
                .build();

        signInButton = (SignInButton) findViewById(R.id.plus_sign_in_button);
        signInButton.setOnClickListener(this);



    }

    @Override
    protected void onStart() {
        super.onStart();
        googleApiClient.connect();
    }

    @Override
    protected void onStop() {

        googleApiClient.disconnect();
        super.onStop();
    }

    private void resolveSignInError() {
        Log.e(TAG, "75");

        if (mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                Log.e(TAG, "80");

                startIntentSenderForResult(mConnectionResult.getResolution().getIntentSender(),
                        RC_SIGN_IN, null, 0, 0, 0);

            } catch (IntentSender.SendIntentException e) {Log.e(TAG, "line 85");
                mIntentInProgress = false;
                googleApiClient.connect();
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_plus_sign_in, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConnected(Bundle bundle) {

        signInClicked = false;

        Log.e(TAG, "line 120");
        String accountName = Plus.AccountApi.getAccountName(googleApiClient);
        SharedPreferences sharedPreferences = getSharedPreferences(SlaniMutibo.PREFS_NAME, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(SlaniMutibo.ACCOUNT_NAME, accountName);
        editor.commit();
        Toast.makeText(this, "User is connected!", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, SlaniMutibo.class);
        Log.e(TAG, "line 128");
        startActivity(intent);
        finish();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        Log.e(TAG, "141");

        if (!mIntentInProgress) {
            mConnectionResult = connectionResult;
            Log.e(TAG, "line 145");

            if (signInClicked) {
                Log.e(TAG, "line 148");
                resolveSignInError();
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e(TAG, "157");

        if (requestCode == RC_SIGN_IN) {
            if (resultCode != RESULT_OK) {
                signInClicked = false; Log.e(TAG, "line 161"); Log.e(TAG, "line 161  " +  resultCode);
            }

            mIntentInProgress = false;

            if (!googleApiClient.isConnecting()) { Log.e(TAG, "line 166");
                googleApiClient.connect();
            }
        }

    }

    @Override
    public void onClick(View v) {
        Log.e(TAG, "174");

        if (!googleApiClient.isConnecting()) {
            Log.e(TAG, "178");
            signInClicked = true;
            resolveSignInError();
        }
    }
}
