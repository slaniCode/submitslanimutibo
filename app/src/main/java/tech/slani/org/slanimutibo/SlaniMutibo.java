package tech.slani.org.slanimutibo;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.google.android.gms.plus.Plus;

import org.apache.http.cookie.Cookie;

import java.io.IOException;

import tech.slani.org.asynctask.GetCookie;


public class SlaniMutibo extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {


    public static final String PREFS_NAME = "MyPrefsFile";
    public static final String ACCOUNT_NAME = "account_name";
    public static final String COOKIE_NAME = "cookie_name";
    public static final String COOKIE_VALUE = "cookie_value";
    public static final String COOKIE_EXPIRATION = "cookie_expiration";
    public static final String TOKEN = "token";
    public static final String LEADERBOARD = "CgkI2fSXzcMPEAIQAw";
    private static final String ACCOUNT_TYPE = "com.google";
    private static final String COOKIE_URL =
            "https://slanimutibo.appspot.com/_ah/login?continue=http://localhost/&auth=";

    private static final String PLAY_FRAGMENT = "play_fragment";

    private static final long COOKE_DEAD = 1000000;
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    private String actiontitle[];

    private SharedPreferences sharedPreferences;
    private String accountName;

    private GoogleApiClient googleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slani_mutibo);

        // check if user is signed in
        sharedPreferences = getSharedPreferences(PREFS_NAME, 0);
        accountName = sharedPreferences.getString(ACCOUNT_NAME, null);
        if (accountName == null) {
            Intent intent = new Intent(this, PlusSignInActivity.class);
            startActivity(intent);
            finish();
        }


        actiontitle = getResources().getStringArray(R.array.navigation_menu);


        if (checkCookie(sharedPreferences.getLong(COOKIE_EXPIRATION, 0))) {
            getCookie(accountName, sharedPreferences.getString(TOKEN, null));
        }

        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Plus.API)
                .addApi(Games.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .addScope(Plus.SCOPE_PLUS_PROFILE)
                .addScope(Games.SCOPE_GAMES)
                .addOnConnectionFailedListener(this)
                .addConnectionCallbacks(this)
                .build();


        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    protected void onStart() {
        super.onStart();
        googleApiClient.connect();
    }

    @Override
    protected void onStop() {
        if (googleApiClient.isConnected()) {
            googleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        switch (position) {
            case 0:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, Play.newInstance())
                        .commit();
                return;
            case 2:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, HistoryFragment.newInstance())
                        .commit();
                return;
            case 1:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, WishListFragment.newInstance())
                        .commit();
                return;
            case 3:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, AddQuestionFragment.newInstance())
                        .commit();
                return;
            case 4:
                startActivityForResult(Games.Leaderboards.getLeaderboardIntent(googleApiClient,
                        LEADERBOARD), 1);
                return;
        }
    }

    public void onSectionAttached(int number) {
        if (actiontitle == null) {
            actiontitle = getResources().getStringArray(R.array.navigation_menu);
        }
        mTitle = actiontitle[number];
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.slani_mutibo, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }



    /* cookie stuff */
    private boolean checkCookie(long epiration) {

        return ((epiration - System.currentTimeMillis()) < COOKE_DEAD);
    }

    private void getCookie(String accountName, String token) {

        Account account = new Account(accountName, ACCOUNT_TYPE);
        AccountManager accountManager = AccountManager.get(
                getApplicationContext());

        if (token != null) {
            accountManager.invalidateAuthToken("ah", null);
        }

        accountManager.getAuthToken(account, "ah", null, this,
                new GetAuthTokenCallback(), null);

    }

    private class GetAuthTokenCallback implements AccountManagerCallback {
        public void run(AccountManagerFuture result) {
            Bundle bundle;
            try {
                bundle = (Bundle) result.getResult();
                Intent intent = (Intent) bundle.get(AccountManager.KEY_INTENT);
                if (intent != null) {
                    startActivity(intent);
                } else {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    String token = bundle.getString(AccountManager.KEY_AUTHTOKEN);
                    editor.putString(TOKEN, token);
                    editor.commit();
                    GetCookie getCookie = new GetCookie(SlaniMutibo.this);
                    getCookie.execute(token);
                }
            } catch (AuthenticatorException e) {
                e.printStackTrace();
            } catch (OperationCanceledException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void setCookie(Cookie cookie) {

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(COOKIE_NAME, cookie.getName());
        editor.putString(COOKIE_VALUE, cookie.getValue());
        editor.putLong(COOKIE_EXPIRATION, cookie.getExpiryDate().getTime());
        editor.commit();
    }
}

