package tech.slani.org.slanimutibo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import retrofit.RestAdapter;
import tech.slani.org.restclient.RestClient;
import tech.slani.org.service.ResponseService;

public class WishListFragment extends Fragment implements AbsListView.OnItemClickListener,
        AbsListView.OnItemLongClickListener,

        LoaderManager.LoaderCallbacks<ArrayList<String>> {


    private OnFragmentInteractionListener mListener;

    /**
     * The fragment's ListView/GridView.
     */
    private AbsListView mListView;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private ArrayAdapter<String> mAdapter;



    static class ViewHolder {
        public TextView title;
    }

    public static WishListFragment newInstance() {
        WishListFragment fragment = new WishListFragment();
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public WishListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_expandable_list_item_1,
                android.R.id.text1);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item2, container, false);


        // Set the adapter
        mListView = (AbsListView) view.findViewById(android.R.id.list);
        ((AdapterView<ListAdapter>) mListView).setAdapter(mAdapter);

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);

        mListView.setOnItemLongClickListener(this);


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
      /*
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        */
        ((SlaniMutibo) activity).onSectionAttached(1);


    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (null != mListener) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

        final String title = (String) mListView.getItemAtPosition(position);

        AlertDialog alert = new AlertDialog.Builder(getActivity())
                .setTitle("Delete entry")
                .setMessage("Are you sure you want to delete " + title + " from wish list?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        deletEntry(title.substring(1, title.length()-1));
                        mAdapter.remove(title);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();

        return false;
    }


    private void deletEntry(String title) {

        Activity activity = getActivity();
        SharedPreferences sharedPreferences =
                activity.getSharedPreferences(SlaniMutibo.PREFS_NAME, 0);
        ResponseService.startActionWishlist(activity, ResponseService.REMOVE_TITLE, title,
                sharedPreferences.getString(SlaniMutibo.COOKIE_NAME, null),
                        sharedPreferences.getString(SlaniMutibo.COOKIE_VALUE, null));
    }
    public void setEmptyText(CharSequence emptyText) {
        View emptyView = mListView.getEmptyView();

        if (emptyView instanceof TextView) {
            ((TextView) emptyView).setText(emptyText);
        }
    }


    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(String id);
    }

    @Override
    public Loader<ArrayList<String>> onCreateLoader(int i, Bundle bundle) {
        return new WishListLoader(getActivity().getApplicationContext());
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<String>> arrayListLoader, ArrayList<String> strings) {
        addAll(strings);
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<String>> arrayListLoader) {

        addAll(null);
    }

    private void addAll(ArrayList<String> stringArrayList) {

        mAdapter.clear();
        if (stringArrayList != null) {
            for (String t : stringArrayList) {
                mAdapter.add(t);
            }
        }
    }

    private static class WishListLoader extends AsyncTaskLoader<ArrayList<String>> {

        final InterestingConfigChanges mLastConfig = new InterestingConfigChanges();

        ArrayList<String> titles;
        private Context context;

        public WishListLoader(Context context) {
            super(context);
            this.context = context;
        }

        @Override
        public ArrayList<String> loadInBackground() {

            RestClient restClientService = new RestAdapter.Builder()
                    .setEndpoint(RestClient.URL)
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .build()
                    .create(RestClient.class);

            SharedPreferences sharedPreferences =
                    context.getSharedPreferences(SlaniMutibo.PREFS_NAME, 0);

            titles = restClientService.getWishList(
                    sharedPreferences.getString(SlaniMutibo.COOKIE_NAME, null) + "=" +
                            sharedPreferences.getString(SlaniMutibo.COOKIE_VALUE, null));


            return titles;
        }

        @Override
        public void deliverResult(ArrayList<String> data) {

            if (isReset()) {
                if (data != null) {
                    onReleaseResources(data);
                }
            }

            ArrayList<String> oldtitles = titles;
            titles = data;

            if (isStarted()) {
                super.deliverResult(data);
            }

            if (oldtitles != null) {
                onReleaseResources(oldtitles);
            }
        }

        @Override
        protected void onStartLoading() {

            if (titles != null) {
                deliverResult(titles);
            }

            boolean configChange = mLastConfig.applyNewConfig(getContext().getResources());

            if (takeContentChanged() || titles == null || configChange) {
                forceLoad();
            }


        }

        @Override
        protected void onStopLoading() {
            cancelLoad();
        }

        @Override
        public void onCanceled(ArrayList<String> data) {
            super.onCanceled(data);

            onReleaseResources(data);
        }

        @Override
        protected void onReset() {
            super.onReset();

            onStartLoading();

            if (titles != null) {
                onReleaseResources(titles);
                titles = null;
            }
        }

        protected void onReleaseResources(ArrayList<String> data) {
        }


    }

}
