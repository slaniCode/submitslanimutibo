package com.example.mymodule.app2;

import com.example.mymodule.repository.HistoryRepo;
import com.example.mymodule.repository.MutiboUserRepo;
import com.example.mymodule.repository.QuestionRepo;
import com.example.mymodule.repository.WishListRepo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration // this object represent a Config. for appl.
@EnableWebMvc  // turn on WebMVC (request can be routed to our controlers)
@ComponentScan ({"com.example.mymodule.*"})// spring will scan our controller
public class Application {

    @Bean
    public QuestionRepo questionRepo() {
        return new QuestionRepo();
    }

    @Bean
    public MutiboUserRepo mutiboUserRepo() {
        return new MutiboUserRepo();
    }

    @Bean
    public HistoryRepo historyRepo() {
        return new HistoryRepo();
    }

    @Bean
    public WishListRepo wishListRepo() {
        return new WishListRepo();
    }
}
