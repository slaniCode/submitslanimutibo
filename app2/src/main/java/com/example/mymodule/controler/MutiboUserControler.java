package com.example.mymodule.controler;

import com.example.mymodule.repoobject.MutiboUser;
import com.example.mymodule.repository.MutiboUserRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by  on 6.11.2014.
 */

@Controller
public class MutiboUserControler {

    @Autowired
    MutiboUserRepo mutiboUserRepo;

    @RequestMapping(value = "/user/add", method = RequestMethod.POST)
    public @ResponseBody String addUser(@RequestBody MutiboUser mutiboUser) {

        mutiboUserRepo.save(mutiboUser);
        return "OK";
    }
}
