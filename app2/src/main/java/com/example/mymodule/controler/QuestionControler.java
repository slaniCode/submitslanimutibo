package com.example.mymodule.controler;

import com.example.mymodule.repoobject.AddSet;
import com.example.mymodule.repoobject.QuestionResponse;
import com.example.mymodule.repoobject.QuestionSet;
import com.example.mymodule.repoobject.UserHistory;
import com.example.mymodule.repository.HistoryRepo;
import com.example.mymodule.repository.QuestionRepo;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Controller
public class QuestionControler {

    private static final String  QUESTION_PATH = "/question/";

    @Autowired
    QuestionRepo questionRepo;
    @Autowired
    HistoryRepo historyRepo;



    @RequestMapping(value = "/", method = RequestMethod.GET)
    public
    @ResponseBody
    String addVideo() {

        UserService userService = UserServiceFactory.getUserService();
        User u = userService.getCurrentUser();
        return u.getEmail();
    }

    @RequestMapping(value =  QUESTION_PATH + "add", method = RequestMethod.POST)
    public
    @ResponseBody
    String addQuestions(@RequestBody List<QuestionSet> list) {
        questionRepo.save(list);
        return "OK";
    }

    @RequestMapping(value =  QUESTION_PATH + "addset", method = RequestMethod.POST)
    public
    @ResponseBody
    String addSet(@RequestBody AddSet addSet) {
        QuestionSet questionSet = new QuestionSet();
        questionSet.setDiffculty(0);
        questionSet.setGrade(0);
        questionSet.setAll(0);
        questionSet.setWrong(0);
        questionSet.setFrequentAnswer(new int[4]);
        int[] answer = new int[4];
        answer[(addSet.getAnswer()-1)] = 1;
        questionSet.setAnswer(answer);
        String[] questions = new String[4];
        questions[0] = addSet.getTitle1();
        questions[1] = addSet.getTitle2();
        questions[2] = addSet.getTitle3();
        questions[3] = addSet.getTitle4();
        questionSet.setQuestions(questions);
        questionSet.setExplanation(addSet.getExplanation());
        questionRepo.save(questionSet);
        return "OK";
    }

    @RequestMapping(value =  QUESTION_PATH + "get", method = RequestMethod.POST)
    public
    @ResponseBody
    List<QuestionSet> getQuestions(@RequestBody int difficulty) {

        return questionRepo.findAll(difficulty);
    }

    @RequestMapping(value =  QUESTION_PATH + "response", method = RequestMethod.POST)
    public
    @ResponseBody
    String response(@RequestBody ArrayList<QuestionResponse> list) {
        Logger logger = Logger.getLogger(QuestionRepo.class.getName());
        logger.warning("uf uf kuzek fuf");
        questionRepo.updateQuestionSet(list);
        return "OK";
    }

    @RequestMapping(value =  QUESTION_PATH + "addhistory", method = RequestMethod.POST)
    public
    @ResponseBody
    String addHistory(@RequestBody UserHistory userHistory) {
        UserService userService = UserServiceFactory.getUserService();
        User u = userService.getCurrentUser();
        userHistory.setUserId(u.getUserId());

        historyRepo.save(userHistory);

        return "OK";
    }

    @RequestMapping(value =  QUESTION_PATH + "gethistory", method = RequestMethod.GET)
    public
    @ResponseBody
    List<UserHistory> getHistory() {

        UserService userService = UserServiceFactory.getUserService();
        User u = userService.getCurrentUser();
        return historyRepo.getUserHistory(u.getUserId());
    }


}
