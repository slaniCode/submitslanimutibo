package com.example.mymodule.controler;

import com.example.mymodule.repository.WishListRepo;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;

/**
 * Created by  on 30.11.2014.
 */

@Controller
public class WishListControler {


    private static final String WISHLIST_PATH = "/wishlist/";

    @Autowired
    private WishListRepo wishlistRepo;

    @RequestMapping(value = WISHLIST_PATH + "addtitle", method = RequestMethod.POST)
    public
    @ResponseBody
    String addTitle(@RequestBody String title) {

        UserService userService = UserServiceFactory.getUserService();
        User u = userService.getCurrentUser();
        wishlistRepo.addMoveTitle(title, u.getUserId());

        return "Ok";
    }

    @RequestMapping(value = WISHLIST_PATH + "removetitle", method = RequestMethod.POST)
    public
    @ResponseBody
    String removeTitle(@RequestBody String title) {

        UserService userService = UserServiceFactory.getUserService();
        User u = userService.getCurrentUser();
        wishlistRepo.removeMovieTitle(title, u.getUserId());

        return "Ok";
    }

    @RequestMapping(value = WISHLIST_PATH + "removewishlist", method = RequestMethod.GET)
    public
    @ResponseBody
    String remove() {

        UserService userService = UserServiceFactory.getUserService();
        User u = userService.getCurrentUser();
        wishlistRepo.removeWishList(u.getUserId());

        return "Ok";
    }

    @RequestMapping(value = WISHLIST_PATH + "getwishlist", method = RequestMethod.GET)
    public
    @ResponseBody
    ArrayList<String> getWishList() {

        UserService userService = UserServiceFactory.getUserService();
        User u = userService.getCurrentUser();

        return wishlistRepo.getWishList(u.getUserId());
    }

}
