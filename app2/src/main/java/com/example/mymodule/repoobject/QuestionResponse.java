package com.example.mymodule.repoobject;

import com.google.appengine.api.datastore.Key;

/**
 * Created by  on 30.10.2014.
 */
public class QuestionResponse {

    private String key;
    private int wrong, grade, answer;

    public void setKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public int getWrong() {
        return wrong;
    }

    public void setWrong(int wrong) {
        this.wrong = wrong;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public int getAnswer() {
        return answer;
    }

    public void setAnswer(int answer) {
        this.answer = answer;
    }
}
