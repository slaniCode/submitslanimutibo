package com.example.mymodule.repoobject;

import org.datanucleus.api.jpa.annotations.Extension;

import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by  on 30.11.2014.
 */
@Entity
public class WishList {

    @Id
    private String userId;
    private ArrayList movieTitle;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public ArrayList<String> getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(ArrayList<String> movieTitle) {
        this.movieTitle = movieTitle;
    }

    public void addTitle(String title) {
        movieTitle.add(title);
    }

    public void removeTitle(String title) {
        movieTitle.remove(title);
    }

    public boolean hasTitle(String title) {

        return  movieTitle.contains(title);
    }
}
