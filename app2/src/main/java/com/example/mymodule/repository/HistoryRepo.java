package com.example.mymodule.repository;

import com.example.mymodule.app2.EMF;
import com.example.mymodule.repoobject.UserHistory;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 * Created by  on 28.11.2014.
 */
public class HistoryRepo extends JPACrudRepo<UserHistory, String> {

    public HistoryRepo() {
        super(UserHistory.class);
    }


    public List<UserHistory> getUserHistory(String userId) {

        EntityManager em = EMF.get().createEntityManager();
        Query query = em.createQuery("SELECT h FROM UserHistory h WHERE h.userId=:userId")
                .setParameter("userId", userId);

        return query.getResultList();
    }


}
