package com.example.mymodule.repository;

import com.example.mymodule.app2.EMF;

import java.io.Serializable;

import javax.persistence.EntityManager;

/**
 * Created by  on 19.10.2014.
 */
public class JPACrudRepo<T, ID extends Serializable> {

    private Class<T> type_;

    public JPACrudRepo(Class<T> type) {

        type_ = type;
    }

    public void save(T entity) {

        EntityManager em = EMF.get().createEntityManager();
        try {
            em.persist(entity);
        } finally {
            em.close();

        }
    }

    public void save(Iterable<T> enities) {

        for (T entitiy : enities) {
            save(entitiy);
        }
    }

}
