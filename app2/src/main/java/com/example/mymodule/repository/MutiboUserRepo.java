package com.example.mymodule.repository;

import com.example.mymodule.app2.EMF;
import com.example.mymodule.repoobject.MutiboUser;
import com.google.appengine.repackaged.com.google.common.base.Flag;

import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 * Created by on 6.11.2014.
 */
public class MutiboUserRepo extends JPACrudRepo<MutiboUser, String> {

    private static final Logger logger = Logger.getLogger(MutiboUserRepo.class.getName());
    public MutiboUserRepo() {
        super(MutiboUser.class);
    }

    public MutiboUser findByName(String username) {

        logger.warning("line 24");
        EntityManager em = EMF.get().createEntityManager();
        Query query = em.createQuery("SELECT u FROM MutiboUser u WHERE u.username =:username")
                .setParameter("username", username);
        logger.warning("line 28");
        if (query == null) {
            return null;
        }
        logger.warning("line 32");

        return (MutiboUser) query.getResultList().get(0);
    }

}
