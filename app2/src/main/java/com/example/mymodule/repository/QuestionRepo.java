package com.example.mymodule.repository;

import com.example.mymodule.app2.EMF;
import com.example.mymodule.repoobject.QuestionResponse;
import com.example.mymodule.repoobject.QuestionSet;
import com.google.appengine.api.datastore.Key;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

/**
 * Created by  on 20.10.2014.
 */
@Service
public class QuestionRepo extends JPACrudRepo<QuestionSet, String> {

    public QuestionRepo() {
        super(QuestionSet.class);
    }

    public List<QuestionSet> findAll(int diffculty) {

        EntityManager em = EMF.get().createEntityManager();
        List<QuestionSet> list;
        try {
            Query query = em.createQuery("SELECT q FROM QuestionSet q WHERE " +
                            "q.diffculty <= :diffculty")
                    .setParameter("diffculty", diffculty);

            list = query.getResultList();
        } finally {
            em.close();
        }

        return list;
    }

    public void updateQuestionSet(QuestionResponse response) {

        EntityManager em = EMF.get().createEntityManager();
        EntityTransaction txn = em.getTransaction();
        txn.begin();
        try {
            QuestionSet qSet = em.find(QuestionSet.class, response.getKey());
            if (qSet != null) {
                qSet.setGrade(qSet.getGrade() + response.getGrade());
                if (qSet.getGrade() < -15) {
                    qSet.setWrong(qSet.getWrong() + (response.getWrong()));
                    qSet.setAll(qSet.getAll() + 1);
                    // this is ugly
                    double all = qSet.getAll();
                    double wrong = qSet.getWrong();
                    qSet.setDiffculty((int) ((wrong / all) * 100));
                    int[] inc = qSet.getFrequentAnswer();
                    inc[response.getAnswer()] += 1;
                    qSet.setFrequentAnswer(inc);

                } else {
                    em.remove(qSet);
                }
            }
            txn.commit();
        } finally {
            if (txn.isActive()) {
                txn.rollback();
            }
        }
    }

    public void updateQuestionSet(ArrayList<QuestionResponse> list) {

        for (QuestionResponse qResponse : list) {
            updateQuestionSet(qResponse);
        }
    }
}

