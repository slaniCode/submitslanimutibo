package com.example.mymodule.repository;

import com.example.mymodule.app2.EMF;
import com.example.mymodule.repoobject.WishList;

import java.util.ArrayList;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 * Created by  on 30.11.2014.
 */
public class WishListRepo extends JPACrudRepo<WishList, String> {

    public WishListRepo() {
        super(WishList.class);
    }

    public void addMoveTitle(String title, String userId) {

        EntityManager em = EMF.get().createEntityManager();
        EntityTransaction txn = em.getTransaction();
        txn.begin();

        try {
            WishList w = em.find(WishList.class, userId);
            if (w != null) {
                if (!w.hasTitle(title))
                    w.addTitle(title);
            } else {
                WishList newW = new WishList();
                newW.setUserId(userId);
                newW.setMovieTitle(new ArrayList<String>());
                newW.addTitle(title);
                em.persist(newW);
            }
            txn.commit();
        } finally {
            if (txn.isActive()) {
                txn.rollback();
            }
        }

    }

    public void removeMovieTitle(String title, String userId) {

        EntityManager em = EMF.get().createEntityManager();
        EntityTransaction txn = em.getTransaction();
        txn.begin();

        try {
            WishList w = em.find(WishList.class, userId);
            if (w != null) {
                if (w.hasTitle(title))
                    w.removeTitle(title);
            }
            txn.commit();
        } finally {
            if (txn.isActive()) {
                txn.rollback();
            }
        }
    }

    public void removeWishList(String userId) {

        EntityManager em = EMF.get().createEntityManager();
        WishList w = em.find(WishList.class, userId);
        if (w != null) {
            em.remove(w);
        }

    }

    public ArrayList<String> getWishList(String userId) {

        EntityManager em = EMF.get().createEntityManager();
        WishList w = em.find(WishList.class, userId);
        if (w != null) {
            return  w.getMovieTitle();
        }
        
        return null;
    }
}



