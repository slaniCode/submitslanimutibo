package com.example.mymodule.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.security.oauth2.provider.client.ClientDetailsUserDetailsService;

/**
 * Created by slani on 12.11.2014.
 */
public class ClientAndUserDetailsService implements ClientDetailsService, UserDetailsService {

    private final ClientDetailsService clients_;

    @Autowired
    private MutiboUserDetailsService users_;

    private final ClientDetailsUserDetailsService clientDetailsWrapper_;

    public ClientAndUserDetailsService(ClientDetailsService clients) {
        super();

        clients_ = clients;
        clientDetailsWrapper_ = new ClientDetailsUserDetailsService(clients_);
    }

    @Override
    public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
        return clients_.loadClientByClientId(clientId);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDetails user = null;
        try {
            user = users_.loadUserByUsername(username);
        } catch (UsernameNotFoundException e){
            user = clientDetailsWrapper_.loadUserByUsername(username);
        }
        return user;
    }
}
