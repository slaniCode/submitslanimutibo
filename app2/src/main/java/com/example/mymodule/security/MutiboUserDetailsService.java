package com.example.mymodule.security;

import com.example.mymodule.repoobject.MutiboUser;
import com.example.mymodule.repository.MutiboUserRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by slani on 6.11.2014.
 */
@Component
public class MutiboUserDetailsService implements UserDetailsService {

    private static final Logger logger = Logger.getLogger(MutiboUserDetailsService.class.getName());
    private final MutiboUserRepo mutiboUserRepo;

    @Autowired
    public MutiboUserDetailsService(MutiboUserRepo mutiboUserRepo) {
        logger.warning("line 30");
        if (mutiboUserRepo == null) {
            throw new IllegalArgumentException("mutiboUserRepo cannot be null");
        }
        this.mutiboUserRepo = mutiboUserRepo;
    }
    

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        logger.warning("line 39");
        MutiboUser mutiboUser = mutiboUserRepo.findByName(username);

        logger.warning("line 41");


        if (mutiboUser == null) {
            throw new UsernameNotFoundException("Invalid username/password.");
        }

        return new MutiboUserDetails(mutiboUser);
    }


    private final class MutiboUserDetails implements UserDetails {

        MutiboUser mutiboUser;
        private MutiboUserDetails(MutiboUser user) {
            mutiboUser = user;
        }

        @Override
        public Collection<? extends GrantedAuthority> getAuthorities() {
            return  AuthorityUtils
                    .createAuthorityList("ROLE_USER");
        }

        @Override
        public String getPassword() {
            return mutiboUser.getPassword();
        }

        @Override
        public String getUsername() {
            return mutiboUser.getUsername();
        }

        @Override
        public boolean isAccountNonExpired() {
            return true;
        }

        @Override
        public boolean isAccountNonLocked() {
            return true;
        }

        @Override
        public boolean isCredentialsNonExpired() {
            return true;
        }

        @Override
        public boolean isEnabled() {
            return true;
        }
    }
}
